package com.amgrade.tracker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
//import android.view.Menu;

public class MainActivity extends Activity implements OnClickListener, LocationListener {
	private static final String LOG_TAG = MainActivity.class.getSimpleName();
	private static final int CAMERA_REQUEST = 1888;
	private static final int WKID = 4326;
	private static final int DIALOG_NO_ID = 1;
	private static final int DIALOG_UPDATE_ERROR = 2;
	private static final int DIALOG_OUT = 3;
	private static final int DIALOG_EXIT = 4;
	private static final int DIALOG_SERVER_ERROR = 5;
	private static final int DIALOG_SERVICE_UNAVAILABLE = 6;
	private static final long TIMEOUT = 30000;
	private static final String BASE_URL = "http://services1.arcgis.com/cb7fSmrHbiudOlZb/ArcGIS/rest/services/Entrants/FeatureServer/0/";
	private static final String URL_QUERY = "query";
	private static final String URL_UPDATE = "updateFeatures";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
	
	private static AsyncHttpClient sClient = new AsyncHttpClient();
	private EditText mField;
	private TextView mSignalView, mLastUpdView;
	private CheckBox mTrackBox;
	private Button mScanBtn;
	private Location mCurrentLocation;
	private Integer mID = 0;
	private boolean mIsTracking = false;
	private TrackingTask mTask;
	private String mProvider;
	private String mUpdateParams;
//	private Calendar mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Criteria cr = new Criteria();
        cr.setAccuracy(Criteria.ACCURACY_FINE);
        mProvider = locationManager.getBestProvider(cr, true);
        locationManager.requestLocationUpdates(mProvider, 0, 0, this);
//        mCalendar = Calendar.getInstance();
        setContentView(R.layout.activity_main);
        mScanBtn = (Button)findViewById(R.id.btn_scan);
        mScanBtn.setOnClickListener(this);
        mField = (EditText)findViewById(R.id.edit_id);
        mField.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				switch(actionId){
				case EditorInfo.IME_ACTION_DONE:
		             RequestParams queryParams = new RequestParams();
		             queryParams.put("where", "RaceNum="+mField.getText().toString());
		             queryParams.put("returnIdsOnly","true");
		             queryParams.put("f","pjson");
		             get(URL_QUERY,queryParams,mQueryResponseHandler);
					break;
				}
				return false;
			}
		});
        mTrackBox = (CheckBox)findViewById(R.id.chk_track);
        mTrackBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					mIsTracking = isChecked;
					if(isChecked && !mField.getText().toString().equals("")){
						mTask = new TrackingTask();
						mTask.execute();
						mField.setEnabled(false);
						mScanBtn.setEnabled(false);
					} /*else {
						mField.setEnabled(true);
						mScanBtn.setEnabled(true);
					}*/
				}
		});
        mSignalView = (TextView)findViewById(R.id.txt_gps_level);
        mLastUpdView = (TextView)findViewById(R.id.txt_last_time);
        sClient.setTimeout(30000);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/


	@Override
	protected void onResume() {
		super.onResume();
		if(mCurrentLocation == null) {
			mCurrentLocation = new Location(LocationManager.GPS_PROVIDER);
		}
		mSignalView.setText(R.string.none);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {
		case CAMERA_REQUEST:
			if(resultCode == RESULT_OK) {
				Bitmap barcode = (Bitmap) data.getExtras().get("data");
				int[] pixels = new int[barcode.getWidth()*barcode.getHeight()];
				barcode.getPixels(pixels, 
						0, 
						barcode.getWidth(), 
						0, 
						0, 
						barcode.getWidth(), 
						barcode.getHeight());
				LuminanceSource source = new RGBLuminanceSource(barcode.getWidth(), 
						barcode.getHeight(), pixels);
				barcode.recycle();
				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		        Reader reader = new MultiFormatReader();
		        try {
		             Result result = reader.decode(bitmap);
		             String raceNum = result.getText();
		             Integer.parseInt(raceNum);
		             mField.setText(raceNum);
		             RequestParams queryParams = new RequestParams();
		             queryParams.put("where", "RaceNum="+raceNum);
		             queryParams.put("returnIdsOnly","true");
		             queryParams.put("f","pjson");
		             get(URL_QUERY,queryParams,mQueryResponseHandler);
		        } catch (NumberFormatException e) {
		        	e.printStackTrace();
		        } catch (NotFoundException e) {
		            e.printStackTrace();
		        } catch (ChecksumException e) {
		            e.printStackTrace();
		        } catch (FormatException e) {
		        	e.printStackTrace();
		        }
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onBackPressed() {
		showAlertDialog(DIALOG_EXIT, null);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_scan:
			Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(cameraIntent, CAMERA_REQUEST);
			break;
		}
		
	}
	
	//Location listener interface
	@Override
	public void onLocationChanged(Location loc) {
//		loc.setLatitude(-41.590797);
//		loc.setLongitude(146.654663);
		mCurrentLocation = loc;
		float accuracy = loc.getAccuracy();
		if(accuracy<0) {
			mSignalView.setText(R.string.none);
		} else if(accuracy>163) {
			mSignalView.setText(R.string.poor);
		} else if(accuracy>48) {
			mSignalView.setText(R.string.avg);
		} else {
			mSignalView.setText(R.string.full);
		}
	}

	@Override
	public void onProviderDisabled(String arg0) {
	}
	@Override
	public void onProviderEnabled(String arg0) {
	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}
	
	//Http requests/responses
	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		sClient.post(BASE_URL+url, params, responseHandler);
		Log.w(LOG_TAG, "POST request:"+BASE_URL+url);
	}
	public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		sClient.get(BASE_URL+url, params, responseHandler);
		Log.w(LOG_TAG, "GET request:"+BASE_URL+url);
	}
	
	private AsyncHttpResponseHandler mQueryResponseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onSuccess(String content) {
			Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
			super.onSuccess(content);
			try {
				JSONObject response = new JSONObject(content);
				JSONArray ids = response.getJSONArray("objectIds");
				Log.w(LOG_TAG, "IDs response: "+content);
				if(ids.length()==0) {
					showAlertDialog(DIALOG_NO_ID, null);
					mTrackBox.setEnabled(false);
					mField.setText("");
				} else {
					mID = (Integer)ids.get(0);
					mTrackBox.setEnabled(true);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public void onFailure(Throwable error, String msg) {
			super.onFailure(error, msg);
			Log.w(LOG_TAG, "IDs response: "+msg);
			showAlertDialog(DIALOG_SERVICE_UNAVAILABLE, null);
			mTrackBox.setEnabled(false);
			
		}
	};
	
	private AsyncHttpResponseHandler mUpdateResponseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onSuccess(String content) {
			super.onSuccess(content);
			Log.w(LOG_TAG, "Update response: "+content);
			Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
			boolean success = false;
			try {
				JSONObject json = new JSONObject(content);
				JSONObject response = new JSONObject(json.getJSONArray("updateResults").getJSONObject(0).toString());
				success = response.getBoolean("success");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(success) {
				Date now = Calendar.getInstance().getTime();
				Log.d(LOG_TAG, "Update successful. Time: "+now);
				mLastUpdView.setText(sdf.format(now));
			} else {
				showAlertDialog(DIALOG_UPDATE_ERROR, content);
				mTrackBox.setChecked(false);
			}
		}
		@Override
		public void onFailure(Throwable error, String msg) {
			super.onFailure(error, msg);
			showAlertDialog(DIALOG_SERVER_ERROR, msg);
			mTrackBox.setChecked(false);
			Log.w(LOG_TAG, "Update response: "+msg);
		}
	};
	
	public void showAlertDialog(int id, String errorMsg) {
		String dialogContent;
		Builder dialogBuilder = new Builder(MainActivity.this)
			.setTitle(R.string.dialog_title)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		switch(id) {
		case DIALOG_NO_ID:
			dialogBuilder.setMessage(R.string.no_id);
			break;
		case DIALOG_OUT:
			dialogContent = getString(R.string.out_of_location)+
					"\nProvider: "+mProvider+
					"\nLatitude: "+mCurrentLocation.getLatitude()+
					"\nLongitude: "+mCurrentLocation.getLongitude();
			dialogBuilder.setMessage(dialogContent);
			break;
		case DIALOG_UPDATE_ERROR:
			dialogContent = getString(R.string.update_error)+
					"\n"+mUpdateParams+
					"\nServer response: "+errorMsg;
			dialogBuilder.setMessage(dialogContent);
//			Log.w(LOG_TAG, "UPDATE_ERROR: "+dialogContent);
			break;
		case DIALOG_EXIT:
			dialogBuilder.setMessage(R.string.exit)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mIsTracking = false;
					MainActivity.this.finish();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			break;
		case DIALOG_SERVER_ERROR:
			dialogContent = getString(R.string.server_error)+
					": "+errorMsg;
			dialogBuilder.setMessage(dialogContent);
//			Log.w(LOG_TAG, "SERVER_ERROR: "+dialogContent);
			break;
		case DIALOG_SERVICE_UNAVAILABLE:
			dialogBuilder.setMessage(R.string.service_unavailable);
			break;
		}
		dialogBuilder.show();
	}
	
	
	private class TrackingTask extends AsyncTask<Void, Integer, Void> {
		private final int OUT_OF_LOCATION = 3;
		private boolean isOut = false;
		@Override
		protected Void doInBackground(Void... params) {
			Log.w(LOG_TAG, "Start tracking");
			long updateTime = Calendar.getInstance().getTimeInMillis();//System.currentTimeMillis();
			int offset = TimeZone.getDefault().getRawOffset();
			/*if(!checkLocation()) {
				publishProgress(OUT_OF_LOCATION);
			} else {
				isOut = false;
			}*/
			updateFeatures(updateTime+offset);
			while(mIsTracking) {
				long newTime = Calendar.getInstance().getTimeInMillis();//System.currentTimeMillis();
				if(newTime-updateTime >= TIMEOUT) {
					/*if(!checkLocation()) {
						publishProgress(OUT_OF_LOCATION);
					} else {
						isOut = false;
					}*/
					Log.w(LOG_TAG, "Update request");
					updateFeatures(newTime+offset);
					updateTime = newTime;
				}
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			switch(values[0]) {
			case OUT_OF_LOCATION:
				if(!isOut) {
					showAlertDialog(DIALOG_OUT, null);
					isOut = true;
//					Log.w(LOG_TAG, "Task finished");
				}
				break;
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Log.w(LOG_TAG, "Task finished");
			mField.setEnabled(true);
			mScanBtn.setEnabled(true);
		}

		private void updateFeatures(long time) {
			if(mCurrentLocation.getLatitude()==0)
				mCurrentLocation.setLatitude(-42.453861);
			if(mCurrentLocation.getLongitude()==0)
				mCurrentLocation.setLongitude(146.191864);
			if(mCurrentLocation.getLatitude()!=0 && mCurrentLocation.getLongitude()!=0) {
//				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
//				Date date = new Date(time);
				RequestParams updateParams = new RequestParams();
				updateParams.put("f", "pjson");
				try {
					JSONObject spRef = new JSONObject();
					spRef.put("wkid", WKID);
					JSONObject geometry = new JSONObject();
					geometry.put("y", mCurrentLocation.getLatitude())
					.put("x", mCurrentLocation.getLongitude())
					.put("spatialReference", spRef);
					JSONObject attrs = new JSONObject();
					attrs.put("Speed", mCurrentLocation.getSpeed())
					.put("LastDate", sdf.format(new Date(time)))
					.put("OBJECTID", mID);
					JSONObject feature = new JSONObject();
					feature.put("geometry", geometry)
					.put("attributes", attrs);
					JSONArray features = new JSONArray();
					features.put(feature);
					updateParams.put("features", features.toString());
//					Log.d("Tracker/UpdateFeatures date", sdf.format(date));
					mUpdateParams = "Params: "+features.toString();
					post(URL_UPDATE, updateParams, mUpdateResponseHandler);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				return;
			}
		}
		
		/*private boolean checkLocation() {
			double x = mCurrentLocation.getLatitude(),
					y = mCurrentLocation.getLongitude();
			if(x==0 && y==0) {
				return true;
			} else if((y < 143.5 || y > 149) && (x < -43.5 || x > -39)) {
				return false;
			} else {
				return true;
			}
		}*/
		
	}

}
